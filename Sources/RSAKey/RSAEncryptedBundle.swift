//
//  RSAEncryptedBundle.swift
//  RSAKey
//
//  Created by Giacomo Leopizzi on 27/04/2017.
//
//

import Foundation

extension RSA {
    
    public struct EncryptedBundle {
        
        let encryptedPayload: Data
        
        let encryptedSimmetricKey: Data
        let iv: Data

    }
    
}
