//
//  NSData-Extension.swift
//  RSAKey
//
//  Created by Giacomo Leopizzi on 27/04/2017.
//
//

import Foundation

internal extension NSData {
    
    var data: Data {
        return self as Data 
    }
    
}
