import XCTest
@testable import RSAKey

// The following priv, pub keys, messages and signature have been generated using openssl

let pemPriv = "-----BEGIN PRIVATE KEY-----\n"
    + "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCfS9UqY3wW6Ncp\n"
    + "73XmKiUaEKHQ2YsrACoHcLjF1tSL4KZCGAOfkC4m6zVtGVvnuGKRGtcK4NRVmUY+\n"
    + "gT4kLixA8SujJ8ZJGmeCCTnbk3R8IQbqhOcZawguaDrY6+t5zLV3/YzxrlazgeXJ\n"
    + "HjSXuo7Q3HpachUCUQPqOm2R4rwfX3qCSWu6M4PRbTPB+8WIewlet0WPoNnuUUTv\n"
    + "hKBDHXAX8sUWBZ3CZ4UCysTJp3KsDITwKHCk/asXHOi0rAi0WucN58DdMEZ2wGuw\n"
    + "e3wnwzTbhSaQn6MDNlR0j7K1ZjMQ17dez4Be+La9w2gadJvezAcX2vODQFjrVpxg\n"
    + "9Zo5WEerAgMBAAECggEADnC8aTRNinIfFzEa+ft2E4/Qa3NdF0/Tf4srZNvdtrRt\n"
    + "ve4ZXfyfAm4uEK7M0wu2+7p4JgdNmFjlskLbFEKPRm/WadrwMa5QFcyqTwpl8962\n"
    + "lsnnfTljq3lNZF62oPr6tF7qyPnp6CntX7b6Q4ro6WwjN1HCL8ySU0hqqF/qd9l2\n"
    + "R/3kewsR4pGYNi4oY3GtRO0ICMZCAAi0v1Tdet8Me1NFGM7sSfcmKV7v9RI5/7lt\n"
    + "z8EOfJbUUBDp5I3GL/wGDa/tjoGOjRDg6iirdBsiEFGIWJXX9hZT8FVLgt+Ai8MB\n"
    + "l+OKR7QOyI7KMm8FtD3xsq+bsEvt3OhDLhGcNEqeMQKBgQDSUk81e6v4TmtC3sbr\n"
    + "elVScI4ZWrs1gyLt9/9xyrK0Kr2ONfk9G6Z4PvyaWAPBHmf0xnsVdoUzcVWhrg42\n"
    + "bJvTYvMNjMxuCykzRhXy9OWNdPM75O0zozBHc8yvq9fIWsNiGXbr3Nw76d+0PRYb\n"
    + "183+Urc5dsH5GvVZ7aKl6xZMgwKBgQDB5I/NJkn4zfbVRrq1iCImginSHO0bSuA5\n"
    + "7B40IGdQkoNnvH93wKz22kBMUAVgX4SGHWbvpKdlo+pw47pFr8zzRrp5jeAHiMmY\n"
    + "CBUPTj78316nULmbi7RvvNdbc5nnZ0lRp+NS6gfcksFTuG5k/42HIIVlgnLRtXxy\n"
    + "IyRRmcJ/uQKBgQDA8zovutG971N/+ZlMluKHyzLSF+b/5Nq6rnXvEyJ3H27fdKy/\n"
    + "XwqN7lsXzf9DwH1mlmB9BEqXMzZ4KZJoY6Nhfrm0iKNToXGe1IF2by3ZZJ1xKUhj\n"
    + "wyabpqT11RUVfg8ZhHsRT4HMhXbxh8ksqgMVexUU5tp1ikHkypoY1V+TuQKBgGYW\n"
    + "/WxsS6iYce3sNuTcT/bstC5wkpu7OgLlgyW5JgzziAL36jnYlnnHgvFrdNlAkdu3\n"
    + "4XouvQE0ZH2aOnr0zLoPNKJKBHqTGGpXXxdXAK1Ow1zfkUsILTJkQRRi8tc3uBAp\n"
    + "kPUYSplmICr/wgil0hQjGHnRTLmEkIjcXgQlLJbpAoGAamlIz3cV12/72zVtF/52\n"
    + "O+5uZ0/4R7FQ1QpY48hkEgjHjMPqqJvOJHfkfyINliHaJ7aernG/qaP+CSukOn5l\n"
    + "3+Kov/ig6G9jlUVijurjLqeneVBVocX90fC65eBDL8H8tEcxfrp6/Jb5iloYtcdB\n"
    + "a6BNdwh9hHOXQ47q28/pjsc=\n"
    + "-----END PRIVATE KEY-----"

let pemPub = "-----BEGIN PUBLIC KEY-----\n"
    + "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAn0vVKmN8FujXKe915iol\n"
    + "GhCh0NmLKwAqB3C4xdbUi+CmQhgDn5AuJus1bRlb57hikRrXCuDUVZlGPoE+JC4s\n"
    + "QPEroyfGSRpnggk525N0fCEG6oTnGWsILmg62Ovrecy1d/2M8a5Ws4HlyR40l7qO\n"
    + "0Nx6WnIVAlED6jptkeK8H196gklrujOD0W0zwfvFiHsJXrdFj6DZ7lFE74SgQx1w\n"
    + "F/LFFgWdwmeFAsrEyadyrAyE8ChwpP2rFxzotKwItFrnDefA3TBGdsBrsHt8J8M0\n"
    + "24UmkJ+jAzZUdI+ytWYzENe3Xs+AXvi2vcNoGnSb3swHF9rzg0BY61acYPWaOVhH\n"
    + "qwIDAQAB\n"
    + "-----END PUBLIC KEY-----"

let msg = "Hello, World!\n"

let sigbase64 = "a9kk3OfrIGOW4NAZnSS6ejTgTr1/NWdmJ86xPv+QX71zlj73jRgymdy3AZgkbWU6mhCWze7tia8hySXnYIPQQ2WLMDjTrPf9jP7maA1jYfNebSx72R2fYUVBNVJirIH/mZKXRD3HobYgF//uq6T3An8Cy80qQ9sjaCqBEsB1uC9w6s3Avrt2aM46BdnvlyDYv9Kr3P34TzXqY658WeDtP/H/22eRm0tzho8ToI1AnF+Knrww/JTjOFjul8RgLFZBduUxKZ7IyMCEAk9YVE1aN5T3Aatjnkbqd7Sf35sEnM3VbFs3o7/E4jsDualC+lEINmEDZUBnllujraozNEJPOA=="

class RSAKeyTests: XCTestCase {
    
    func testRSAGenerateSignVerify() throws {
        // Tests generation, signing and verification of signature using and RSA 2048bit key and sha256
        let key = RSAKey.generate(keyLength: 1024 * 8)
        let sig = try RSA.sign(message: Data([UInt8](msg.utf8)), key: key)
        print(key.pubKey!)
        print(sig.base64EncodedString(options: []))
        XCTAssertTrue(try RSA.verify(message: Data([UInt8](msg.utf8)), signature: sig, key: key))
    }
    
    func testRSASignVerify() throws {
        // Tests signing and verification of signature using and RSA 2048bit key and sha256
        let keySign = try RSAKey(pemPrivString: pemPriv)
        let sig = try RSA.sign(message: Data([UInt8](msg.utf8)), key: keySign)
        print(keySign.pubKey!)
        print(sig.base64EncodedString(options: []))
        let keyVerif = try RSAKey(pemPubString: pemPub)
        print(keyVerif.pubKey!)
        XCTAssertTrue(try RSA.verify(message: Data([UInt8](msg.utf8)), signature: sig, key: keyVerif))
        
    }
    
    func testRSAPrivPubKeys() throws {
        // Tests key generation from priv.pem and pub.pem lead to same pub key
        let keySign = try RSAKey(pemPrivString: pemPriv)
        let keyVerif = try RSAKey(pemPubString: pemPub)
        XCTAssertTrue(keyVerif.pubKey! == keySign.pubKey!)
        
    }
    
    func testRSALoadPrivSign() throws {
        // Tests that message signed with privKey generates the expected signature
        let key = try RSAKey(pemPrivString: pemPriv)
        let sig = try RSA.sign(message: Data([UInt8](msg.utf8)), key: key)
        XCTAssertEqual(sig.base64EncodedString(options: []) , sigbase64) // TODO: compare sig to signature
    }
    
    func testRSALoadPubVerify() throws {
        // Tests that the signature for the message signed with privKey is properly verified by the pubKey
        let key = try RSAKey(pemPubString: pemPub)
        let d = Data(base64Encoded: sigbase64, options: .ignoreUnknownCharacters)!
        XCTAssertTrue(try RSA.verify(message: Data([UInt8](msg.utf8)), signature: d, key: key))
    }
    
    func testRSAEncryptionDecryption() throws {
        
        let originalString = pemPub
        
        let key = try RSAKey(pemPubString: pemPub)
        let data = originalString.data(using: .utf8)! as NSData
        
        let result = try RSA.encrypt(data: data, withKey: key, usingCipher: .aes_256_cbc, padding: RSA.Padding.none)
        
        print(result.encryptedPayload.base64EncodedString())
        
        print("Plaintext lenght: \(data.length)")
        print("Encrypted Payload lenght: \(result.encryptedPayload.count)")
        print("Encrypted Key lenght: \(result.encryptedSimmetricKey.count)")
        print("IV lenght: \(result.iv.count)")
        
        
        let privateKey = try RSAKey(pemPrivString: pemPriv)
        
        let result2 = try RSA.decrypt(bundle: result, withKey: privateKey, usingCipher: .aes_256_cbc)
        
        guard let string = String(data: result2 as Data, encoding: .utf8) else {
            XCTAssert(false)
            return
        }
        print("The original message: \(string)")
        
        XCTAssert(originalString == string, "String check")
    }
    
    func testStress() throws {
        try autoreleasepool {
            while true {
                let originalString = pemPub
                
                let key = try RSAKey(pemPubString: pemPub)
                let data = originalString.data(using: .utf8)! as NSData
                
                let result = try RSA.encrypt(data: data, withKey: key, usingCipher: .aes_256_cbc, padding: RSA.Padding.none)
                              
                let privateKey = try RSAKey(pemPrivString: pemPriv)
                
                let result2 = try RSA.decrypt(bundle: result, withKey: privateKey, usingCipher: .aes_256_cbc)
                
                guard let _ = String(data: result2 as Data, encoding: .utf8) else {
                    XCTAssert(false)
                    return
                }


            }
        }
    }
    
    static var allTests : [(String, (RSAKeyTests) -> () throws -> Void)] {
        return [
            ("testRSAGenerateSignVerify", testRSAGenerateSignVerify),
            ("testRSASignVerify", testRSASignVerify),
            ("testRSAPrivPubKeys", testRSAPrivPubKeys),
            ("testRSALoadPrivSign", testRSALoadPrivSign),
            ("testRSALoadPubVerify", testRSALoadPubVerify),
            ("testRSAEncryptionDecryption", testRSAEncryptionDecryption),
        ]
    }
}

