import XCTest
@testable import RSAKeyTests

XCTMain([
    testCase(RSAKeyTests.allTests),
])
