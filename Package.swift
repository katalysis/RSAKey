// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "RSAKey",
    products: [
    .library(name: "RSAKey", targets: ["RSAKey"]),
    ],
    dependencies: [

        .package(url: "https://github.com/apple/swift-nio-ssl.git", from: "1.0.0"),
//.Package(url: "https://github.com/Zewo/COpenSSL.git", majorVersion: 0, minor: 14),
//    .Package(url: "https://github.com/Zewo/OpenSSL.git", majorVersion: 0, minor: 14),
    //.Package(url: "https://github.com/Zewo/Axis.git", majorVersion: 0, minor: 14),
    ],
    targets: [
    .target(name: "RSAKey", dependencies: ["NIOOpenSSL"]),
    .testTarget(name: "RSAKeyTests", dependencies: ["RSAKey"])
    ]
)
